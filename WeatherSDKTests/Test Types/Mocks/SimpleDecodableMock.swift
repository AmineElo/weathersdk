//
//  SimpleDecodableMock.swift
//  WeatherSDKTests
//
//  Created by Amine Elouattar on 10/3/2023.
//

import Foundation

struct SimpleDecodableMock: Decodable{
    let property: String
}

extension SimpleDecodableMock: Equatable{
    public static func ==(lhs: SimpleDecodableMock, rhs: SimpleDecodableMock) -> Bool {
        return lhs.property == rhs.property
    }
}
