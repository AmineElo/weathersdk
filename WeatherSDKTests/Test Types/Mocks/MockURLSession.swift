//
//  MockURLSession.swift
//  WeatherSDKTests
//
//  Created by Amine Elouattar on 10/3/2023.
//

@testable import WeatherSDK
import Foundation

class MockURLSession: URLSessionProtocol {
    var queue: DispatchQueue? = nil
    
    func givenMockDispatchQueue() {
      queue = DispatchQueue(label: "com.WeatherSDK.MockSession")
    }

    func makeDataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionTaskProtocol {
      return MockURLSessionTask(completionHandler: completionHandler, urlRequest: request, queue: queue)
    }
    
}

class MockURLSessionTask: URLSessionTaskProtocol {
    
    var completionHandler: (Data?, URLResponse?, Error?) -> Void
    var urlRequest: URLRequest
    
    init(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void, urlRequest: URLRequest, queue: DispatchQueue?) {
        if let queue = queue {
            self.completionHandler = { data, response, error in
                queue.async() {
                  completionHandler(data, response, error)
                }
            }
        }else{
            self.completionHandler = completionHandler
        }
        
        self.urlRequest = urlRequest
    }
  
    var calledResume = false
    func resume() {
        calledResume = true
    }
}
