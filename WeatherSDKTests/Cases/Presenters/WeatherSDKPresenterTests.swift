//
//  WeatherSDKPresenterTests.swift
//  WeatherSDKTests
//
//  Created by Amine Elouattar on 10/3/2023.
//

import XCTest
@testable import WeatherSDK

final class WeatherSDKPresenterTests: XCTestCase {
    
    var sut: WeatherSDKPresenter!
    
    let cityWithAState = City(name: "Washington", lat: 38.8950368, lon: -77.0365427, country: "US", state: "District of Columbia")
    let cityWithoutAState = City(name: "Casablanca", lat: 33.5950627, lon: -7.6187768, country: "MA", state: "")

    override func setUpWithError() throws {
        try super.setUpWithError()
        
        let interactor = MockedInteractor()
        let configuration = ApiConfiguration(appid: "")
        
        self.sut = WeatherSDKPresenter(interactor: interactor, configuration: configuration)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        self.sut = nil
    }
    
    func testconstruct_cities_from_a_valid_WSResponse_without_state() throws{
        let data = try Data.fromJSON(fileName: "CityWithoutAState")
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode([DirectGeocodingRS].self, from: data)
        
        let result = sut.constructCitiesFromWSResponse(mockedResponse)
        
        XCTAssertEqual(result, [cityWithoutAState])
    }
    
    func testconstruct_cities_from_a_valid_WSResponse_with_state() throws{
        let data = try Data.fromJSON(fileName: "CityWithState")
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode([DirectGeocodingRS].self, from: data)
        
        let result = sut.constructCitiesFromWSResponse(mockedResponse)
        
        XCTAssertEqual(result, [cityWithAState])
    }
    
    func testConstruct_current_forecast_from_a_valid_WSResponse() throws{
        let data = try Data.fromJSON(fileName: "currentWeather")
        
        let weatherInfo = WeatherInfo(id: 803, description: "broken clouds", icon: "04d")
        
        let currentWeather = WeatherForecast(city: cityWithAState, weather: [weatherInfo], currentTemperature: 12, feelsLikeTemperature: 11, minTemparture: 9, maxTemparture: 14)
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode(CurrentWeatherDataRS.self, from: data)
        
        let result = try sut.constructCurrentForecastFromWSResponse(mockedResponse, city: cityWithAState)
        
        XCTAssertEqual(result, currentWeather)
    }
    
    func testConstruct_current_forecast_with_missing_fields_WSResponse() throws{
        let data = try Data.fromJSON(fileName: "incompleteCurrentWeather")
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode(CurrentWeatherDataRS.self, from: data)
        
        do{
            let _ = try sut.constructCurrentForecastFromWSResponse(mockedResponse, city: cityWithAState)
        }catch WeatherSDKError.missingData{
            XCTAssertTrue(true)
        }
        
    }
    
    func testConstruct_hourly_forecast_from_a_valid_WSResponse() throws{
        let data = try Data.fromJSON(fileName: "hourlyForecast")
        
        let hourlyForecast = createHourlyForecaseObject()
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode(WeatherForecastRS.self, from: data)
        
        let result = try sut.constructHourlyForecastFromWSResponse(mockedResponse, city: cityWithAState)
        
        XCTAssertEqual(result, hourlyForecast)
    }
    
    func testConstruct_current_hourly_forecast_with_missing_fields_WSResponse() throws{
        let data = try Data.fromJSON(fileName: "incompleteHourlyForecast")
        
        let decoder = JSONDecoder()
        let mockedResponse = try decoder.decode(WeatherForecastRS.self, from: data)
        
        do{
            let _ = try sut.constructHourlyForecastFromWSResponse(mockedResponse, city: cityWithAState)
        }catch WeatherSDKError.missingData{
            XCTAssertTrue(true)
        }
        
    }
    
    
    func createHourlyForecaseObject() -> HourlyForecast{
        
        var forecast = [Forecast]()
        
        forecast.append(Forecast(weather: [WeatherInfo(id: 803, description: "broken clouds", icon: "04n")], currentTemperature: 9, feelsLikeTemperature: 9, minTemparture: 7, maxTemparture: 9, date: Date(timeIntervalSince1970: 1678471200), dateString: "2023-03-10 18:00:00"))
        
        forecast.append(Forecast(weather: [WeatherInfo(id: 803, description: "broken clouds", icon: "04n")], currentTemperature: 9, feelsLikeTemperature: 9, minTemparture: 7, maxTemparture: 9, date: Date(timeIntervalSince1970: 1678471200), dateString: "2023-03-10 18:00:00"))
        
        return HourlyForecast(count: 2, list: forecast, city: cityWithAState)
    }
    
    internal class MockedInteractor: InteractorProtocol{
        func executeRequest<T>(fromURL urlString: String, queryParams: [String : String]?, httpMethod: HttpMethod, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
            
        }
    }
}
