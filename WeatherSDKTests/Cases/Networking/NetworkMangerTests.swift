//
//  NetworkMangerTests.swift
//  WeatherSDKTests
//
//  Created by Amine Elouattar on 10/3/2023.
//

@testable import WeatherSDK
import XCTest

final class NetworkMangerTests: XCTestCase {
    
    var mockSession: MockURLSession!
    var sut: NetworkManager!
    
    var url: URL {
        return URL(string: "https://google.com")!
    }

    override func setUpWithError() throws {
        try super.setUpWithError()
        mockSession = MockURLSession()
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        mockSession = nil
        sut = nil
    }
    
    typealias requestCompletion<T: Decodable> = (calledCompletion: Bool, results: T?, error: Error?)
    
    func whenBuildRequest(data: Data? = nil, queryParams: [String: String]? = nil, statusCode: Int = 200, error: Error? = nil) -> requestCompletion<SimpleDecodableMock> {
        
        let response = HTTPURLResponse(url: url,
                                       statusCode: statusCode,
                                       httpVersion: nil,
                                       headerFields: nil)
        var calledCompletion = false
        var results: SimpleDecodableMock? = nil
        var receivedError: Error? = nil
        
        sut = NetworkManager(session: mockSession, responseQueue: nil)
        let mockTask = sut.buildTask(fromURL: url.absoluteString, queryParams: queryParams, httpMethod: .get) { (result: Result<SimpleDecodableMock, Error>) in
            calledCompletion = true
            switch result{
            case .success(let object):
                results = object
            case .failure(let error):
                receivedError = error
            }
        } as! MockURLSessionTask
            
        mockTask.completionHandler(data, response, error)
        return (calledCompletion, results, receivedError)
    }
    
    func verifyExecuteRequestDispatchedToMain(data: Data? = nil, statusCode: Int = 200, queryParams: [String: String]? = nil, error: Error? = nil) {

        mockSession.givenMockDispatchQueue()
        sut = NetworkManager(session: mockSession, responseQueue: .main)
        
        let expectation = self.expectation(description: "Completion wasn't called")

        var thread: Thread!
        let mockTask = sut.buildTask(fromURL: url.absoluteString, queryParams: queryParams, httpMethod: .get) { (result: Result<SimpleDecodableMock, Error>) in
            thread = Thread.current
            expectation.fulfill()
        } as! MockURLSessionTask

        let response = HTTPURLResponse(url: url,
                                     statusCode: statusCode,
                                     httpVersion: nil,
                                     headerFields: nil)
        mockTask.completionHandler(data, response, error)

        waitForExpectations(timeout: 0.2) { _ in
            XCTAssertTrue(thread.isMainThread)
        }
    }

    
    func test_executeRequest_givenResponseStatusCode500_callsCompletion() {
        let result = whenBuildRequest(statusCode: 500)

        XCTAssertTrue(result.calledCompletion)
        XCTAssertNil(result.results)
        XCTAssertNotNil(result.error)
    }
    
    func testexecuteRequest_givenError_callsCompletionWithError() throws {
        let expectedError = NSError(domain: "com.WeatherSDK", code: 42)

        let result = whenBuildRequest(error: expectedError)

        XCTAssertTrue(result.calledCompletion)
        XCTAssertNil(result.results)
        let actualError = try XCTUnwrap(result.error as NSError?)
        XCTAssertEqual(actualError, expectedError)
    }
    
    func test_executeRequest_givenValidJSON_callsCompletionWithDecodable() throws {
        let data = try Data.fromJSON(fileName: "SimpleMock")
        
        let decoder = JSONDecoder()
        let decodable = try decoder.decode(SimpleDecodableMock.self, from: data)
        
        let result = whenBuildRequest(data: data)
        
        XCTAssertTrue(result.calledCompletion)
        XCTAssertEqual(result.results, decodable)
        XCTAssertNil(result.error)
    }
    
    func test_executeRequest_givenInvalidJSON_callsCompletionWithError() throws {
        
        let data = try Data.fromJSON(fileName: "SimpleMockMissingValue")

        var expectedError: NSError!
        let decoder = JSONDecoder()
        do {
            _ = try decoder.decode(SimpleDecodableMock.self, from: data)
        } catch {
            expectedError = error as NSError
        }

        // when
        let result = whenBuildRequest(data: data)

        // then
        XCTAssertTrue(result.calledCompletion)
        XCTAssertNil(result.results)

        let actualError = try XCTUnwrap(result.error as NSError?)
        XCTAssertEqual(actualError.domain, expectedError.domain)
        XCTAssertEqual(actualError.code, expectedError.code)
    }
    
    func test_executeRequest_givenHTTPStatusError_dispatchesToResponseQueue() {
        verifyExecuteRequestDispatchedToMain(statusCode: 500)
    }
    
    func test_executeRequest_givenError_dispatchesToResponseQueue() {
        let error = NSError(domain: "com.WeatherError", code: 42)

        verifyExecuteRequestDispatchedToMain(error: error)
    }
    
    func test_executeRequest_givenGoodResponse_dispatchesToResponseQueue() throws {
        let data = try Data.fromJSON(fileName: "SimpleMock")
        
        verifyExecuteRequestDispatchedToMain(data: data)
    }
    
    func test_executeRequest_givenInvalidResponse_dispatchesToResponseQueue() throws {
        let data = try Data.fromJSON(fileName: "SimpleMockMissingValue")
        
        verifyExecuteRequestDispatchedToMain(data: data)
    }
}
