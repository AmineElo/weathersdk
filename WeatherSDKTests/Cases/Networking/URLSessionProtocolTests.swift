//
//  URLSessionProtocolTests.swift
//  WeatherSDKTests
//
//  Created by Amine Elouattar on 10/3/2023.
//

@testable import WeatherSDK
import XCTest

class URLSessionProtocolTests: XCTestCase {
  var session: URLSession!
  var urlRequest: URLRequest!
  
  override func setUp() {
    super.setUp()
    session = URLSession(configuration: .default)
    urlRequest = URLRequest(url: URL(string: "https://example.com")!)
  }
  
  override func tearDown() {
    session = nil
    urlRequest = nil
    super.tearDown()
  }
  
  func test_URLSessionTask_conformsTo_URLSessionTaskProtocol() {
      
    let task = session.dataTask(with: urlRequest)
    
    XCTAssertTrue((task as AnyObject) is URLSessionTaskProtocol)
  }
  
  func test_URLSession_conformsTo_URLSessionProtocol() {
    XCTAssertTrue((session as AnyObject) is URLSessionProtocol)
  }
  
  func test_URLSession_makeDataTask_creates_task_with_passedInRequest() {
    let task = session.makeDataTask(
      with: urlRequest,
      completionHandler: { _, _, _ in })
    as! URLSessionTask

    XCTAssertEqual(task.originalRequest, urlRequest)
  }
  
  func test_URLSession_makeDataTask_creates_task_with_passedInCompletion() {
    let expectation =
      expectation(description: "Completion should be called")

    let task = session.makeDataTask(
      with: urlRequest,
      completionHandler: { _, _, _ in expectation.fulfill() })
    as! URLSessionTask
    task.cancel()

    waitForExpectations(timeout: 0.2, handler: nil)
  }
}
