//
//  Weather.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

struct Weather: Decodable{
    let id: Int
    let main: String
    let description: String
    let icon: String
}
