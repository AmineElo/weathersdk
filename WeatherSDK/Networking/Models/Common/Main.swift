//
//  Main.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

struct Main: Decodable {
    let temp: Decimal?
    let feelsLike: Decimal?
    let tempMin: Decimal?
    let tempMax: Decimal?
    let pressure: Decimal?
    let humidity: Decimal?
    let seaLevel: Decimal?
    let grndLevel: Decimal?
    
    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case humidity
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
    }
}
