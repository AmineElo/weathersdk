//
//  Wind.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

struct Wind: Decodable {
    let speed: Decimal?
    let deg: Decimal?
    let gust: Decimal?
}
