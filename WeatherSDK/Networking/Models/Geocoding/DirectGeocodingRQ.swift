//
//  DirectGeocodingRQ.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

struct DirectGeocodingRQ: Encodable {
    let query: String
    let appid: String
    let limit: Int?
    
    enum CodingKeys: String, CodingKey {
        case query = "q"
        case appid
        case limit
    }
}
