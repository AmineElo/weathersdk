//
//  DirectGeocodingRS.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

struct DirectGeocodingRS: Decodable{
    let name: String
    let lat: Double
    let lon: Double
    let country: String
    let state: String?
}
