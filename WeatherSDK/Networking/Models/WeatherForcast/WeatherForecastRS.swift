//
//  WeatherForecastRS.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

struct WeatherForecastRS: Decodable{
    let cnt: Int
    let list: [ForecastItem]
}

struct ForecastItem: Decodable{
    let dt: Int
    let dtTxt: String
    let main: Main
    let weather: [Weather]
    let wind: Wind?
    
    enum CodingKeys: String, CodingKey{
        case dt
        case dtTxt = "dt_txt"
        case main
        case weather
        case wind
    }
}
