//
//  CurrentWeatherDataRS.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

struct CurrentWeatherDataRS: Decodable{
    let id: Int
    let weather: [Weather]
    let main: Main
    let wind: Wind
    let name: String
}
