//
//  CurrentWeatherDataRQ.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

struct CurrentWeatherDataRQ: Encodable {
    let lat: Double
    let lon: Double
    let appid: String
    let units: MeasurementUnit = .metric
}
