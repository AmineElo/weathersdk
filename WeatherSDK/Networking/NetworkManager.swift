//
//  NetworkManager.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

protocol WeatherSDKNetworkProvider {
    func buildTask<T: Decodable>(fromURL urlString: String,
                               queryParams: [String: String]?,
                               httpMethod: HttpMethod,
                               completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTaskProtocol
}

class NetworkManager: NSObject {
    
    static let sharedInstance = NetworkManager(responseQueue: .main)
    private var session: URLSessionProtocol!
    private var responseQueue: DispatchQueue?
    
    enum ManagerErrors: Error {
        case invalidResponse
        case invalidStatusCode(Int)
    }
    
    
    init(session: URLSessionProtocol? = nil, responseQueue: DispatchQueue?) {
        self.responseQueue = responseQueue
        super.init()
        
        if let session = session {
            self.session = session
        }else{
            let configuration = URLSessionConfiguration.default
            self.session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        }
        
        
    }
    
    func request<T: Decodable>(fromURL urlString: String,
                               queryParams: [String: String]? = nil,
                               httpMethod: HttpMethod = .get,
                               completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTaskProtocol {
        
        let completionOnQueue: (Result<T, Error>) -> Void = { result in
            guard let responseQueue = self.responseQueue else {
                completion(result)
                return
            }
            responseQueue.async {
                completion(result)
            }
        }
        
        var urlComponents = URLComponents(string: urlString)!
        
        if let queryParams = queryParams{
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in queryParams{
                urlComponents.queryItems?.append(URLQueryItem(name: key, value: value))
            }
        }
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = httpMethod.method

        let dataTask = self.session.makeDataTask(with: request) { data, response, error in
            if let error = error {
                completionOnQueue(.failure(error))
                return
            }

            guard let urlResponse = response as? HTTPURLResponse else { return completionOnQueue(.failure(ManagerErrors.invalidResponse)) }
            if !(200..<300).contains(urlResponse.statusCode) {
                return completionOnQueue(.failure(ManagerErrors.invalidStatusCode(urlResponse.statusCode)))
            }

            guard let data = data else { return }
            do {
                let decodedResponse = try JSONDecoder().decode(T.self, from: data)
                completionOnQueue(.success(decodedResponse))
            } catch {
                completionOnQueue(.failure(error))
            }
        }

        return dataTask
    }
}

extension NetworkManager: WeatherSDKNetworkProvider {
    func buildTask<T>(fromURL urlString: String, queryParams: [String : String]?, httpMethod: HttpMethod, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTaskProtocol where T : Decodable{
        return self.request(fromURL: urlString, queryParams: queryParams, httpMethod: httpMethod, completion: completion)
    }
}
///:nodoc:
extension NetworkManager: URLSessionDelegate{}
