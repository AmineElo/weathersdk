//
//  URLSessionProtocol.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 10/3/2023.
//

import Foundation

protocol URLSessionProtocol: AnyObject {
  
  func makeDataTask(
    with request: URLRequest,
    completionHandler:
      @escaping (Data?, URLResponse?, Error?) -> Void)
  -> URLSessionTaskProtocol
}

protocol URLSessionTaskProtocol: AnyObject {
  func resume()
}

extension URLSessionTask: URLSessionTaskProtocol { }

extension URLSession: URLSessionProtocol {
  
  func makeDataTask(
    with request: URLRequest,
    completionHandler:
      @escaping (Data?, URLResponse?, Error?) -> Void)
  -> URLSessionTaskProtocol {
    return dataTask(with: request,
                    completionHandler: completionHandler)
  }
}
