//
//  WeatherSDKPresenter.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

struct ApiConfiguration {
    var appid: String
}

class WeatherSDKPresenter{
    
    fileprivate var interactor: InteractorProtocol
    fileprivate var configuration: ApiConfiguration?
    
    init(interactor: InteractorProtocol = WeatherSDKInteractor(), configuration: ApiConfiguration?) {
        self.interactor = interactor
        self.configuration = configuration
    }
    
    func updateConfiguration(configuration: ApiConfiguration){
        self.configuration = configuration
    }
    
    func retrieveCitySuggestions(with query: String, limit: Int, completion: @escaping ([City], WeatherSDKError?)->Void){
        
        guard let configuration = configuration else {
            completion([], .noSetup)
            return
        }
        
        let limit = limit > 5 ? 5 : limit
        let params = DirectGeocodingRQ(query: query, appid: configuration.appid, limit: limit)
        
        self.interactor.executeRequest(fromURL: Constants.API.directGeocoding, queryParams: params.dictionary, httpMethod: .get) { (result: Result<[DirectGeocodingRS], Error>) in
            switch result {
            case .success(let response):
                let cities = self.constructCitiesFromWSResponse(response)
                completion(cities, nil)
            case .failure(_):
                completion([], .serverCommunication)
            }
        }
    }
    
    func constructCitiesFromWSResponse(_ response: [DirectGeocodingRS]) -> [City]{
        return response.reduce(into: [City]()) { $0.append(City(name: $1.name, lat: $1.lat, lon: $1.lon, country: $1.country, state: $1.state ?? "")) }
    }
    
    public func retrieveCurrentWeatherData(with city: City, completion: @escaping (WeatherForecast?, WeatherSDKError?)->Void){
        
        guard let configuration = configuration else {
            completion(nil, .noSetup)
            return
        }
        
        let params = CurrentWeatherDataRQ(lat: city.lat, lon: city.lon, appid: configuration.appid)
        
        self.interactor.executeRequest(fromURL: Constants.API.curentWeatherData, queryParams: params.dictionary, httpMethod: .get) { (result: Result<CurrentWeatherDataRS, Error>) in
            switch result {
            case .success(let response):
                do{
                    let currentForecast = try self.constructCurrentForecastFromWSResponse(response, city: city)
                    completion(currentForecast, nil)
                }catch let error as WeatherSDKError{
                    completion(nil, error)
                }catch{
                    completion(nil, .unexpected(code: 0))
                }
            case .failure(_):
                completion(nil, .serverCommunication)
            }
        }
    }
    
    func constructCurrentForecastFromWSResponse(_ response: CurrentWeatherDataRS, city: City) throws -> WeatherForecast{
        
        guard let currentTemp = response.main.temp?.int,
              let feelsLikeTemp = response.main.feelsLike?.int,
              let minTemp = response.main.tempMin?.int,
              let maxTemp = response.main.tempMax?.int
        
        else {throw WeatherSDKError.missingData}
        
        let weatherInfo = response.weather.reduce(into: [WeatherInfo]()) {$0.append(WeatherInfo(id: $1.id, description: $1.description, icon: $1.icon))}
        
        return WeatherForecast(city: city, weather: weatherInfo, currentTemperature: currentTemp, feelsLikeTemperature: feelsLikeTemp, minTemparture: minTemp, maxTemparture: maxTemp)
    }
    
    public func retrieveWeatherHourlyForecast(with city: City, completion: @escaping (HourlyForecast?, WeatherSDKError?)->Void){
        
        guard let configuration = configuration else {
            completion(nil, .noSetup)
            return
        }
        
        let params = CurrentWeatherDataRQ(lat: city.lat, lon: city.lon, appid: configuration.appid)
        
        self.interactor.executeRequest(fromURL: Constants.API.weatherForecast, queryParams: params.dictionary, httpMethod: .get) { (result: Result<WeatherForecastRS, Error>) in
            switch result {
            case .success(let response):
                do{
                    let currentForecast = try self.constructHourlyForecastFromWSResponse(response, city: city)
                    completion(currentForecast, nil)
                }catch let error as WeatherSDKError{
                    completion(nil, error)
                }catch{
                    completion(nil, .unexpected(code: 0))
                }
            case .failure(_):
                completion(nil, .serverCommunication)
            }
        }
    }
    
    func constructHourlyForecastFromWSResponse(_ response: WeatherForecastRS, city: City) throws -> HourlyForecast{
        
        var hourlyForecast = [Forecast]()
        
        for item in response.list {
            
            guard let currentTemp = item.main.temp?.int,
                  let feelsLikeTemp = item.main.feelsLike?.int,
                  let minTemp = item.main.tempMin?.int,
                  let maxTemp = item.main.tempMax?.int
            else {throw WeatherSDKError.missingData}
            
            let date = Date(timeIntervalSince1970: TimeInterval(item.dt))
            
            let weatherInfo = item.weather.reduce(into: [WeatherInfo]()) {$0.append(WeatherInfo(id: $1.id, description: $1.description, icon: $1.icon))}
            
            hourlyForecast.append(Forecast(weather: weatherInfo,currentTemperature: currentTemp, feelsLikeTemperature: feelsLikeTemp, minTemparture: minTemp, maxTemparture: maxTemp, date: date, dateString: item.dtTxt))
        }
        
        return HourlyForecast(count: response.cnt, list: hourlyForecast, city: city)
    }

}

