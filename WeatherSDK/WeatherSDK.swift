//
//  WeatherSDK.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

public class WeatherSDK {
    
    /// Shared instance to library
    public static let standard = WeatherSDK()
    
    fileprivate let presenter: WeatherSDKPresenter
    fileprivate var configuration: ApiConfiguration?
    
    private init() {
        self.presenter = WeatherSDKPresenter(configuration: self.configuration)
    }
    
    /// Setup parameters for SDK. This is mandatory before accessing PersonalClient.sharedInstance
    /// - Parameter appid: Your unique openweathermap API key
    public func setup(appid: String){
        let configuration = ApiConfiguration(appid: appid)
        self.configuration = configuration
        self.presenter.updateConfiguration(configuration: configuration)
    }
    
    /// Retrieve City suggestions with the query
    /// - Parameters:
    ///   - query: City name, state code (only for the US) and country code divided by comma. Please use ISO 3166 country codes.
    ///   - limit: Number of the locations in the response (up to 5 results can be returned with this method).
    ///   The default value is 1.
    ///   - completion: The completion block to execute when a query is executed.
    public func retrieveCitySuggestions(with query: String, limit: Int = 1, completion: @escaping ([City], WeatherSDKError?)->Void){
        self.presenter.retrieveCitySuggestions(with: query, limit: limit, completion: completion)
    }
    
    /// Retrieve a city's current weather data
    /// - Parameters:
    ///   - city: city of the data to get.
    ///   - completion: The completion block to execute when a current data are retrieved.
    public func retrieveCurrentWeatherData(with city: City, completion: @escaping (WeatherForecast?, WeatherSDKError?)->Void){
        self.presenter.retrieveCurrentWeatherData(with: city, completion: completion)
    }
    
    /// Retrieve 5 day forecast from any location on the globe. It includes weather forecast data with 3-hour step
    /// - Parameters:
    ///   - city: city of the data to get.
    ///   - completion: The completion block to execute when a forecast data are retrieved.
    public func retrieveWeatherHourlyForecast(with city: City, completion: @escaping (HourlyForecast?, WeatherSDKError?)->Void){
        self.presenter.retrieveWeatherHourlyForecast(with: city, completion: completion)
    }
}
