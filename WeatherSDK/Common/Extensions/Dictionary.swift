//
//  Dictionary.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

extension Dictionary {
    func dictionaryQueryString()-> String {
        let urlParams:String = self.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }).joined(separator: "&");
        
        return urlParams;
    }
}
