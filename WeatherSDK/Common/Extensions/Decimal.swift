//
//  Decimal.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

extension Decimal {
    var int: Int {
        return NSDecimalNumber(decimal: self).intValue
    }
}
