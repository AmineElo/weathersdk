//
//  Enums.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

public enum MeasurementUnit: String, Codable{
    case standard
    case metric
    case imperial
}

public enum WeatherSDKError: Error {
    case noSetup
    case serverCommunication
    case missingData
    case unexpected(code: Int)
}

extension WeatherSDKError{
    public var description: String {
        switch self{
        case .noSetup:
            return "You must initialize SDK with the app id."
        case .serverCommunication:
            return "There was an error communicating with the server."
        case .missingData:
            return "There was a missing data from the server call."
        case .unexpected(_):
            return "An unexpected error occurred."
        }
    }
}

enum HttpMethod: String {
    case get
    case post
    var method: String { rawValue.uppercased() }
}
