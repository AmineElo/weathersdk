//
//  Constants.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

struct Constants {
    struct API {
        static let directGeocoding = "http://api.openweathermap.org/geo/1.0/direct"
        static let zipGeocoding = "http://api.openweathermap.org/geo/1.0/zip"
        static let curentWeatherData = "https://api.openweathermap.org/data/2.5/weather"
        static let weatherForecast = "https://api.openweathermap.org/data/2.5/forecast"
    }
}
