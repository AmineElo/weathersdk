//
//  City.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 8/3/2023.
//

import Foundation

/// City Object
public struct City{
    ///City name
    public let name: String
    /// City geo location, latitude
    public let lat: Double
    /// City geo location, longitude
    public let lon: Double
    /// Country code
    public let country: String
    /// City state, if it's available
    public let state: String
    
    public init(name: String, lat: Double, lon: Double, country: String, state: String) {
        self.name = name
        self.lat = lat
        self.lon = lon
        self.country = country
        self.state = state
    }
}

extension City: Equatable {
    public static func ==(lhs: City, rhs: City) -> Bool {
        return lhs.name == rhs.name && lhs.lat == rhs.lat && lhs.lon == rhs.lon && lhs.country == rhs.country && lhs.state == rhs.state
    }
}
