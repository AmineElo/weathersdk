//
//  WeatherInfo.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

public struct WeatherInfo {
    /// Weather condition id
    public let id: Int
    ///  Weather condition  (Rain, Snow, Extreme etc.)
    public let description: String
    /// Weather icon id
    public let icon: String
}

extension WeatherInfo: Equatable {
    public static func ==(lhs: WeatherInfo, rhs: WeatherInfo) -> Bool {
        return lhs.id == rhs.id && lhs.description == rhs.description && lhs.icon == rhs.icon
    }
}
