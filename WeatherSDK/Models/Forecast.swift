//
//  Forecast.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

public struct Forecast{
    /// weather Info array
    public let weather: [WeatherInfo]
    /// Current Temperature
    public let currentTemperature: Int
    /// Temperature. This temperature accounts for the human perception of weather.
    public let feelsLikeTemperature: Int
    /// Minimum temperature at the moment. This is minimal currently observed temperature (within large megalopolises and urban areas).
    public let minTemparture: Int
    /// Maximum temperature at the moment. This is maximal currently observed temperature (within large megalopolises and urban areas).
    public let maxTemparture: Int
    /// Time of data forecasted, UTC
    public let date: Date
    /// Time of data forecasted, ISO, UTC
    public let dateString: String
}

extension Forecast: Equatable {
    public static func ==(lhs: Forecast, rhs: Forecast) -> Bool {
        return lhs.weather == rhs.weather && lhs.currentTemperature == rhs.currentTemperature && lhs.feelsLikeTemperature == rhs.feelsLikeTemperature && lhs.minTemparture == rhs.minTemparture && lhs.maxTemparture == rhs.maxTemparture && lhs.date == rhs.date && lhs.dateString == rhs.dateString
    }
}
