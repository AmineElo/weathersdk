//
//  HourlyForecast.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

public struct HourlyForecast {
    /// Number of forecast items.
    public let count: Int
    /// List of forecast items.
    public let list: [Forecast]
    /// City object
    public let city: City
}

extension HourlyForecast: Equatable {
    public static func ==(lhs: HourlyForecast, rhs: HourlyForecast) -> Bool {
        return lhs.count == rhs.count && lhs.list == rhs.list && lhs.city == rhs.city
    }
}
