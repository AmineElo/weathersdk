//
//  WeatherForecast.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

public struct WeatherForecast{
    /// city object
    public let city: City
    /// weather Info array
    public let weather: [WeatherInfo]
    /// Current Temperature
    public let currentTemperature: Int
    /// Temperature. This temperature accounts for the human perception of weather.
    public let feelsLikeTemperature: Int
    /// Minimum temperature at the moment. This is minimal currently observed temperature (within large megalopolises and urban areas).
    public let minTemparture: Int
    /// Maximum temperature at the moment. This is maximal currently observed temperature (within large megalopolises and urban areas).
    public let maxTemparture: Int
}

extension WeatherForecast: Equatable {
    public static func ==(lhs: WeatherForecast, rhs: WeatherForecast) -> Bool {
        return lhs.city == rhs.city && lhs.currentTemperature == rhs.currentTemperature && lhs.feelsLikeTemperature == rhs.feelsLikeTemperature && lhs.minTemparture == rhs.minTemparture && lhs.maxTemparture == rhs.maxTemparture
    }
}
