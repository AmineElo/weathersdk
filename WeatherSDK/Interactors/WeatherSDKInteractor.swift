//
//  WeatherSDKInteractor.swift
//  WeatherSDK
//
//  Created by Amine Elouattar on 9/3/2023.
//

import Foundation

protocol InteractorProtocol {
    func executeRequest<T: Decodable>(fromURL urlString: String,
                               queryParams: [String: String]?,
                               httpMethod: HttpMethod,
                               completion: @escaping (Result<T, Error>) -> Void)
}

struct WeatherSDKInteractor: InteractorProtocol {
    fileprivate var networkProvider: WeatherSDKNetworkProvider
    
    init(networkProvider: WeatherSDKNetworkProvider = NetworkManager.sharedInstance) {
        self.networkProvider = networkProvider
    }
    
    func executeRequest<T: Decodable>(fromURL urlString: String, queryParams: [String: String]?, httpMethod: HttpMethod, completion: @escaping (Result<T, Error>) -> Void){
        let task = self.networkProvider.buildTask(fromURL: urlString, queryParams: queryParams, httpMethod: httpMethod, completion: completion)
        task.resume()
    }
}
