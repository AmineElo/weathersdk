# ``WeatherSDK``

The weather SDK accelerates and simplifies development of iOS Weather apps, by providing ready to use methods to retrieve Weather data.

## Quick start

**Include SDK in your app**

* Download the SDK framework, and put it in the root of your project.
* Drag the framework to your app (Embedded) Frameworks

**Include and setup WeatherSDK in your App's entry point.**

```swift
import UIKit
import WeatherSDK

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        WeatherSDK.standard.setup(appid: "YOUR_APPID")
        return true
    }
}
```

**Retrieve city suggestions with a given query**

```swift
    searchText = ...
    WeatherSDK.standard.retrieveCitySuggestions(with: searchText) { cities, error in
        if cities.count > 0 {
            ...
        }
    }
```

**Retrieve Current forecast of a city**

```swift
    WeatherSDK.standard.retrieveCurrentWeatherData(with: city) { currentForecast, error in
        if let currentForecast = currentForecast {
            ...
        }
    }
```

**Retrieve Hourly forecast of a city**

```swift
    WeatherSDK.standard.retrieveWeatherHourlyForecast(with: city) { hourlyForecast, error in
        if let hourlyForecast = hourlyForecast {
            ...
        }
    }
```

## Sequence Diagram

The following is a generic diagram that explains the typical interaction between the app, the SDK and API.

![A generic sequence diagram of weather SDK methods](SDK_Sequence_Diagram)
